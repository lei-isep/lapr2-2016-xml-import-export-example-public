package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class EventCentre {
	@XmlElement
	private UserSet confirmedUsers;

	@XmlElement
	private UserSet confirmationPendingUsers;

	@XmlElement
	private Stands stands;

	@XmlElement
	private ExhibitionSet exhibitionSet;


	public EventCentre(UserSet confirmedUsers, UserSet confirmationPendingUsers, Stands stands, ExhibitionSet exhibitionSet) {
		this.confirmedUsers = confirmedUsers;
		this.confirmationPendingUsers = confirmationPendingUsers;
		this.stands = stands;
		this.exhibitionSet = exhibitionSet;
	}

	private EventCentre() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof EventCentre)) {
			return false;
		}

		EventCentre that = (EventCentre) o;

		if (!confirmedUsers.equals(that.confirmedUsers)) {
			return false;
		}
		if (!confirmationPendingUsers.equals(that.confirmationPendingUsers)) {
			return false;
		}
		if (!stands.equals(that.stands)) {
			return false;
		}
		return exhibitionSet.equals(that.exhibitionSet);

	}

	@Override
	public int hashCode() {
		int result = confirmedUsers.hashCode();
		result = 31 * result + confirmationPendingUsers.hashCode();
		result = 31 * result + stands.hashCode();
		result = 31 * result + exhibitionSet.hashCode();
		return result;
	}
}
