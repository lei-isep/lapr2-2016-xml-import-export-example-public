package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class FAESet {

	@XmlElement(name = "fae")
	private List<FAE> faes;

	private FAESet() {
	}

	public FAESet(List<FAE> faes) {
		this.faes = faes;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof FAESet)) {
			return false;
		}

		FAESet that = (FAESet) o;

		return faes.equals(that.faes);

	}

	@Override
	public int hashCode() {
		return faes.hashCode();
	}
}
