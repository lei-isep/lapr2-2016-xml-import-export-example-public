package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class FAE {

	@XmlElement
	private User user;

	public FAE(User user) {
		this.user = user;
	}

	private FAE() {
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof FAE)) {
			return false;
		}

		FAE fae = (FAE) o;

		return user.equals(fae.user);

	}

	@Override
	public int hashCode() {
		return user.hashCode();
	}

}
