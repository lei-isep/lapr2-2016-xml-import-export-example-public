package lapr.project.model;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
@XmlRootElement
public class Review {

	@XmlElement
	private String text;

	@XmlElement
	private Integer faeTopicKnowledge;

	@XmlElement
	private Integer eventAdequacy;

	@XmlElement
	private Integer workshopAdequacy;

	@XmlElement
	private Integer inviteAdequacy;

	@XmlElement
	private Integer recommendation;

	@XmlElement
	private Assignment assignment;

	private Review() {
	}

	public Review(String text, Integer faeTopicKnowledge, Integer eventAdequacy, Integer workshopAdequacy, Integer inviteAdequacy, Integer recommendation, Assignment assignment) {
		this.text = text;
		this.faeTopicKnowledge = faeTopicKnowledge;
		this.eventAdequacy = eventAdequacy;
		this.workshopAdequacy = workshopAdequacy;
		this.inviteAdequacy = inviteAdequacy;
		this.recommendation = recommendation;
		this.assignment = assignment;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) {
			return true;
		}
		if (!(o instanceof Review)) {
			return false;
		}

		Review review = (Review) o;

		if (!text.equals(review.text)) {
			return false;
		}
		if (!faeTopicKnowledge.equals(review.faeTopicKnowledge)) {
			return false;
		}
		if (!eventAdequacy.equals(review.eventAdequacy)) {
			return false;
		}
		if (!workshopAdequacy.equals(review.workshopAdequacy)) {
			return false;
		}
		if (!inviteAdequacy.equals(review.inviteAdequacy)) {
			return false;
		}
		if (!recommendation.equals(review.recommendation)) {
			return false;
		}
		return assignment.equals(review.assignment);

	}

	@Override
	public int hashCode() {
		int result = text.hashCode();
		result = 31 * result + faeTopicKnowledge.hashCode();
		result = 31 * result + eventAdequacy.hashCode();
		result = 31 * result + workshopAdequacy.hashCode();
		result = 31 * result + inviteAdequacy.hashCode();
		result = 31 * result + recommendation.hashCode();
		result = 31 * result + assignment.hashCode();
		return result;
	}

	public Assignment getAssignment() {
		return assignment;
	}
}
