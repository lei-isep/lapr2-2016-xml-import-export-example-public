package lapr.project.utils;

import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by nuno on 28/05/17.
 */
public class StringUtilTest {

	@Test
	public void ensureLineBreakIsCorrect() throws Exception {
		assertEquals(String.format("%n"), new StringUtil().getLineBreak());
	}
}