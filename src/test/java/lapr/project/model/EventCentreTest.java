package lapr.project.model;

import lapr.project.utils.XMLParser;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVParser;
import org.apache.commons.csv.CSVRecord;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Before;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static junit.framework.TestCase.assertEquals;
import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
public class EventCentreTest {
	private List<String> keywordsList = new ArrayList<>();

	@Test
	public void ensureConvertToXmlFileWorks() throws Exception {
		String utilizadorExportFilePath = "target/test-classes/CentroExposicoesOutput.xml";
		String expectedCentroExposicoesImportFilePath = "target/test-classes/CentroExposicoesImportExample_v0.3.xml";

		List<User> utilizadoresConfirmadosList = new ArrayList<>();
		User userConfirmado1 = new User("João Confirmado1", "joao1@empresa.pt", "joao1@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado1);
		User userConfirmado2 = new User("João Confirmado2", "joao2@empresa.pt", "joao2@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado2);

		User faeUser1 = new User("Fae1", "fae1@centro.pt", "fae1@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser1);

		User faeUser2 = new User("Fae2", "fae2@centro.pt", "fae2@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser2);

		User userOrganizador = new User("Manuel", "manuel@centro.pt", "manuel@centro.pt", "password");
		utilizadoresConfirmadosList.add(userOrganizador);

		List<User> utilizadoresNaoConfirmadosList = new ArrayList<>();
		User userNaoConfirmado = new User("João não confirmado", "joao@empresa.pt", "joao@empresa.pt", "password");
		utilizadoresNaoConfirmadosList.add(userNaoConfirmado);

		// Lista de Organizadores
		List<Organiser> organizadoresList = new ArrayList<>();
		Organiser organiser = new Organiser(userOrganizador);
		organizadoresList.add(organiser);
		OrganiserSet organizadores = new OrganiserSet(organizadoresList);

		// Lista de FAESet
		List<FAE> faesList = new ArrayList<>();

		FAE fae1 = new FAE(faeUser1);
		faesList.add(fae1);
		FAE fae2 = new FAE(faeUser2);
		faesList.add(fae2);
		FAESet FAESet = new FAESet(faesList);

		List<String> keywordsCandidatura1 = new ArrayList<>();
		keywordsCandidatura1.add("Keyword1");
		keywordsCandidatura1.add("Keyword2");

		List<String> keywordsCandidatura2 = new ArrayList<>();
		keywordsCandidatura2.add("Keyword2");
		keywordsCandidatura2.add("Keyword3");

		/*Avaliacoes avaliacoes = new Avaliacoes(avaliacoesList);*/
		Application application1 = new Application(Boolean.TRUE, "application 1 description", 20, 5, new ArrayList<>(), keywordsCandidatura1);
		Application application2 = new Application(Boolean.FALSE, "application 2 description", 10, 15, new ArrayList<>(), keywordsCandidatura2);

		List<Assignment> atribuicoesList = new ArrayList<>();
		ArrayList<Review> avaliacoesList = new ArrayList<>();

		Assignment assignment1 = new Assignment(fae1);//, application1);
		Review review1 = new Review("text avaliação 1", 3, 5, 2, 2, 4, assignment1);

		Assignment assignment2 = new Assignment(fae2);//, application1);
		Review review2 = new Review("text avaliação 2", 2, 3, 1, 5, 3, assignment2);

		atribuicoesList.add(assignment1);
		atribuicoesList.add(assignment2);

		avaliacoesList.add(review1);
		avaliacoesList.add(review2);

		application1.setReviews(avaliacoesList);

		AssignmentSet assignmentSet = new AssignmentSet(atribuicoesList);

		//Submission submissao = new Submission(dataInicioSubmissao, dataFimSubmissao);

		// Criar exposição
		String tituloExposicao = new String("ExpoJaneiro");
		String descricaoExposicao = new String("ExpoJaneiro");

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		Date dataInicioExposicao = sdf.parse("01/10/2017");
		Date dataFimExposicao = sdf.parse("01/20/2017");
		Date dataInicioSubmissao = sdf.parse("12/01/2016");
		Date dataFimSubmissao = sdf.parse("12/01/2016");
		Date dataLimiteConflitos = sdf.parse("01/05/2017");

		String localExposicao = new String("Porto");

		List<Application> listaApplications = new ArrayList<>();
		listaApplications.add(application1);
		listaApplications.add(application2);

		ApplicationSet applicationSet = new ApplicationSet(listaApplications);

		Exhibition exhibition = new Exhibition(tituloExposicao, descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, applicationSet, assignmentSet);

		List<Exhibition> exposicoesList = new ArrayList<>();
		exposicoesList.add(exhibition);
		ExhibitionSet exhibitionSet = new ExhibitionSet(exposicoesList);

		UserSet userSetConfirmados = new UserSet(utilizadoresConfirmadosList);
		UserSet userSetNaoConfirmados = new UserSet(utilizadoresNaoConfirmadosList);

		// Lista de stands
		Stand stand1 = new Stand("Stand 1");
		Stand stand2 = new Stand("Stand 2");
		List<Stand> standsList = new ArrayList<>();
		standsList.add(stand1);
		standsList.add(stand2);

		Stands stands = new Stands(standsList);
		EventCentre eventCentre = new EventCentre(userSetConfirmados, userSetNaoConfirmados, stands, exhibitionSet);

		File file = new File(utilizadorExportFilePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(eventCentre, file);
		jaxbMarshaller.marshal(eventCentre, System.out);

		XMLParser xmlParser = new XMLParser();

		Node expected = xmlParser.readXMLElementFromFile(utilizadorExportFilePath);

		Node result = xmlParser.readXMLElementFromFile(expectedCentroExposicoesImportFilePath);

		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreWhitespace(true);
		assertXMLEqual(expected.getOwnerDocument(), result.getOwnerDocument());

	}

	@Test
	public void convertFromCSVtoObjectToXML() throws Exception {
		String startFile = "target/test-classes/CentroExposicoesAssessment_v0.1.xml";
		String outFile = "target/test-classes/Full-Conversion.xml";

		File file = new File(startFile);
		JAXBContext jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		EventCentre eventCentre = (EventCentre) jaxbUnmarshaller.unmarshal(file);

		file = new File(outFile);
		jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(eventCentre, file);
		//jaxbMarshaller.marshal(eventCentre, System.out);

		XMLParser xmlParser = new XMLParser();

		Node expected = xmlParser.readXMLElementFromFile(outFile);

		Node result = null;
		result = xmlParser.readXMLElementFromFile(startFile);

		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreWhitespace(true);
		assertXMLEqual(expected.getOwnerDocument(), result.getOwnerDocument());
	}

	@Test
	public void convertFromCSVtoXML() throws IOException, JAXBException, ParseException {
		String startFile = "target/test-classes/stats_to_xml.csv";
		String outFile = "target/test-classes/CentroExposicoesAssessment_output.xml";

		Reader in = new FileReader(startFile);
		//final URL url = new URL(startFile);


		Reader reader = new InputStreamReader(new FileInputStream(startFile), "UTF-8");

		//Reader reader = new FileReader(startFile,;

		//final Reader reader = new InputStreamReader(new BOMInputStream(in.), "UTF-8");

		/*Iterable<CSVRecord> records = CSVFormat.RFC4180.withHeader("Exposição","Aceite","Descrição","Área pretendida",
				"Convites","texto", "ConhecimentoFae",
				"Adequação Exposição", "Adequação Demonstrações","Adequação Convites",
				"Recomendacao","Nome","email","username","password","texto2",
				"ConhecimentoFae2","Adequação Exposição2","Adequação Demonstrações2","Adequação Convites2",
				"Recomendacao2","Nome2","email2","username2","password2").parse(in);
*/
		//		Iterable<CSVRecord> records = CSVFormat.EXCEL.withFirstRecordAsHeader().parse(reader);


		final CSVParser parser = new CSVParser(reader, CSVFormat.EXCEL.withHeader().withDelimiter(';'));

		// Criar exposição
		String tituloExposicao = new String("ExpoJaneiro");
		String descricaoExposicao = new String("ExpoJaneiro");

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);

		Date dataInicioExposicao = sdf.parse("01/10/2017");
		Date dataFimExposicao = sdf.parse("01/20/2017");
		Date dataInicioSubmissao = sdf.parse("12/01/2016");
		Date dataFimSubmissao = sdf.parse("12/01/2016");
		Date dataLimiteConflitos = sdf.parse("01/05/2017");

		String localExposicao = new String("Porto");


		List<User> utilizadoresConfirmadosList = new ArrayList<>();

		List<Application> listaApplications = new ArrayList<>();

		ApplicationSet applicationSet = new ApplicationSet(listaApplications);

		User userOrganizador = new User("Manuel", "manuel@centro.pt", "manuel@centro.pt", "password");
		utilizadoresConfirmadosList.add(userOrganizador);

		List<Organiser> organizadoresList = new ArrayList<>();
		Organiser organiser = new Organiser(userOrganizador);
		organizadoresList.add(organiser);
		OrganiserSet organizadores = new OrganiserSet(organizadoresList);

		// Lista de FAESet
		List<FAE> faesList = new ArrayList<>();

		User faeUser1 = new User("Fae1", "fae1@centro.pt", "fae1@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser1);

		User faeUser2 = new User("Fae2", "fae2@centro.pt", "fae2@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser2);

		User faeUser3 = new User("Fae3", "fae3@centro.pt", "fae3@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser3);

		User faeUser4 = new User("Fae4", "fae4@centro.pt", "fae4@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser4);

		User faeUser5 = new User("Fae5", "fae5@centro.pt", "fae5@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser5);

		FAE fae1 = new FAE(faeUser1);
		faesList.add(fae1);
		FAE fae2 = new FAE(faeUser2);
		faesList.add(fae2);
		FAE fae3 = new FAE(faeUser3);
		faesList.add(fae3);
		FAE fae4 = new FAE(faeUser4);
		faesList.add(fae4);
		FAE fae5 = new FAE(faeUser5);
		faesList.add(fae5);
		FAESet FAESet = new FAESet(faesList);

		List<Assignment> atribuicoesList = new ArrayList<>();
		AssignmentSet assignmentSet = new AssignmentSet(atribuicoesList);


		UserSet userSetConfirmados = new UserSet(utilizadoresConfirmadosList);


		List<User> utilizadoresNaoConfirmadosList = new ArrayList<>();
		User userNaoConfirmado = new User("João não confirmado", "joao@empresa.pt", "joao@empresa.pt", "password");
		utilizadoresNaoConfirmadosList.add(userNaoConfirmado);


		UserSet userSetNaoConfirmados = new UserSet(utilizadoresNaoConfirmadosList);

		// Lista de stands
		Stand stand1 = new Stand("Stand 1");
		Stand stand2 = new Stand("Stand 2");
		List<Stand> standsList = new ArrayList<>();
		standsList.add(stand1);
		standsList.add(stand2);

		Stands stands = new Stands(standsList);

		Exhibition exhibition1 = new Exhibition("exposicão1", descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, new ApplicationSet(new ArrayList<>()), new AssignmentSet(new ArrayList<>()));

		Exhibition exhibition2 = new Exhibition("exposicão2", descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, new ApplicationSet(new ArrayList<>()), new AssignmentSet(new ArrayList<>()));

		Exhibition exhibition3 = new Exhibition("exposicão3", descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, new ApplicationSet(new ArrayList<>()), new AssignmentSet(new ArrayList<>()));

		Exhibition exhibition4 = new Exhibition("exposicão4", descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, new ApplicationSet(new ArrayList<>()), new AssignmentSet(new ArrayList<>()));

		Exhibition exhibition5 = new Exhibition("exposicão5", descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, new ApplicationSet(new ArrayList<>()), new AssignmentSet(new ArrayList<>()));

		List<Exhibition> exposicoesList = new ArrayList<>();
		exposicoesList.add(exhibition1);
		exposicoesList.add(exhibition2);
		exposicoesList.add(exhibition3);
		exposicoesList.add(exhibition4);
		exposicoesList.add(exhibition5);


		ExhibitionSet exhibitionSet = new ExhibitionSet(exposicoesList);

		EventCentre eventCentre = new EventCentre(userSetConfirmados, userSetNaoConfirmados, stands, exhibitionSet);

		//List<User> utilizadores = new ArrayList<>();

		for (CSVRecord record : parser) {
			Application application = getCandidatura(record);
			application.toString();

			String nomeExposicao = record.get("Exposição");
			if ("exposição1".equals(nomeExposicao)) {
				exhibition1.addCandidatura(application);
			}
			if ("exposição2".equals(nomeExposicao)) {
				exhibition2.addCandidatura(application);
			}
			if ("exposição3".equals(nomeExposicao)) {
				exhibition3.addCandidatura(application);
			}
			if ("exposição4".equals(nomeExposicao)) {
				exhibition4.addCandidatura(application);
			}
			if ("exposição5".equals(nomeExposicao)) {
				exhibition5.addCandidatura(application);
			}
		}
		File file = new File(outFile);
		JAXBContext jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(eventCentre, file);
		//jaxbMarshaller.marshal(eventCentre, System.out);
	}

	private Application getCandidatura(CSVRecord record) {
		Boolean aceite = new Boolean(Boolean.valueOf(record.get("Aceite")));
		String descricao = record.get("Descrição");
		Integer area = Integer.parseInt(record.get("Área pretendida"));
		Integer convites = Integer.parseInt(record.get("Convites"));

		Random rand = new Random();
		int n = rand.nextInt(keywordsList.size()) + 1;

		List<String> keywords = new ArrayList<>();

		n = rand.nextInt(keywordsList.size());
		keywords.add(keywordsList.get(n));

		n = rand.nextInt(keywordsList.size());
		keywords.add(keywordsList.get(n));

		n = rand.nextInt(keywordsList.size());
		keywords.add(keywordsList.get(n));

		Application application = new Application(aceite, descricao, area, convites, new ArrayList<>(), keywords);

		ArrayList<Review> avaliacoesList = new ArrayList<>();

		avaliacoesList.add(getAvaliacao1(record));
		avaliacoesList.add(getAvaliacao2(record));


		application.setReviews(avaliacoesList);
		return application;
	}

	private User getUtilizador1(CSVRecord record) {
		String nome = record.get("Nome");
		String email = record.get("email");
		String username = record.get("username");
		String password = record.get("password");

		return new User(nome, email, username, password);
	}

	private User getUtilizador2(CSVRecord record) {
		String nome = record.get("Nome2");
		String email = record.get("email2");
		String username = record.get("username2");
		String password = record.get("password2");

		return new User(nome, email, username, password);
	}

	private Review getAvaliacao1(CSVRecord record) {
		String texto = record.get("texto");
		Integer conhecimento = Integer.parseInt(record.get("ConhecimentoFae"));
		Integer exposicao = Integer.parseInt(record.get("Adequação Exposição"));
		Integer demonstracao = Integer.parseInt(record.get("Adequação Demonstrações"));
		Integer convites = Integer.parseInt(record.get("Adequação Convites"));
		Integer recomendacao = Integer.parseInt(record.get("Recomendacao"));

		FAE fae = new FAE(getUtilizador1(record));

		Assignment assignment = new Assignment(fae);//, candidatura1);

		return new Review(texto, conhecimento, exposicao, demonstracao, convites, recomendacao, assignment);
	}

	private Review getAvaliacao2(CSVRecord record) {
		String texto = record.get("texto2");
		Integer conhecimento = Integer.parseInt(record.get("ConhecimentoFae2"));
		Integer exposicao = Integer.parseInt(record.get("Adequação Exposição2"));
		Integer demonstracao = Integer.parseInt(record.get("Adequação Demonstrações2"));
		Integer convites = Integer.parseInt(record.get("Adequação Convites2"));
		Integer recomendacao = Integer.parseInt(record.get("Recomendacao2"));

		FAE fae = new FAE(getUtilizador2(record));

		Assignment assignment = new Assignment(fae);//, candidatura1);

		return new Review(texto, conhecimento, exposicao, demonstracao, convites, recomendacao, assignment);
	}

	@Test
	public void ensureConvertToCSVFileWorks() throws Exception {
		String utilizadorExportFilePath = "target/test-classes/CentroExposicoesOutput.csv";
		String expectedCentroExposicoesImportFilePath = "target/test-classes/CentroExposicoesImportExample_v0.3.xml";

		List<User> utilizadoresConfirmadosList = new ArrayList<>();
		User userConfirmado1 = new User("João Confirmado1", "joao1@empresa.pt", "joao1@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado1);
		User userConfirmado2 = new User("João Confirmado2", "joao2@empresa.pt", "joao2@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado2);

		User faeUser1 = new User("Fae1", "fae1@centro.pt", "fae1@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser1);

		User faeUser2 = new User("Fae2", "fae2@centro.pt", "fae2@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser2);

		User userOrganizador = new User("Manuel", "manuel@centro.pt", "manuel@centro.pt", "password");
		utilizadoresConfirmadosList.add(userOrganizador);

		List<User> utilizadoresNaoConfirmadosList = new ArrayList<>();
		User userNaoConfirmado = new User("João não confirmado", "joao@empresa.pt", "joao@empresa.pt", "password");
		utilizadoresNaoConfirmadosList.add(userNaoConfirmado);

		// Lista de Organizadores
		List<Organiser> organizadoresList = new ArrayList<>();
		Organiser organiser = new Organiser(userOrganizador);
		organizadoresList.add(organiser);
		OrganiserSet organizadores = new OrganiserSet(organizadoresList);

		// Lista de FAESet
		List<FAE> faesList = new ArrayList<>();

		FAE fae1 = new FAE(faeUser1);
		faesList.add(fae1);
		FAE fae2 = new FAE(faeUser2);
		faesList.add(fae2);
		FAESet FAESet = new FAESet(faesList);

		List<String> keywordsCandidatura1 = new ArrayList<>();
		keywordsCandidatura1.add("Keyword1");
		keywordsCandidatura1.add("Keyword2");

		List<String> keywordsCandidatura2 = new ArrayList<>();
		keywordsCandidatura2.add("Keyword2");
		keywordsCandidatura2.add("Keyword3");

		/*Avaliacoes avaliacoes = new Avaliacoes(avaliacoesList);*/
		Application application1 = new Application(Boolean.TRUE, "application 1 description", 20, 5, new ArrayList<>(), keywordsCandidatura1);
		Application application2 = new Application(Boolean.FALSE, "application 2 description", 10, 15, new ArrayList<>(), keywordsCandidatura2);

		List<Assignment> atribuicoesList = new ArrayList<>();
		ArrayList<Review> avaliacoesList = new ArrayList<>();

		Assignment assignment1 = new Assignment(fae1);//, application1);
		Review review1 = new Review("text avaliação 1", 3, 5, 2, 2, 4, assignment1);

		Assignment assignment2 = new Assignment(fae2);//, application1);
		Review review2 = new Review("text avaliação 2", 2, 3, 1, 5, 3, assignment2);

		atribuicoesList.add(assignment1);
		atribuicoesList.add(assignment2);

		avaliacoesList.add(review1);
		avaliacoesList.add(review2);

		application1.setReviews(avaliacoesList);

		AssignmentSet assignmentSet = new AssignmentSet(atribuicoesList);

		//Submission submissao = new Submission(dataInicioSubmissao, dataFimSubmissao);

		// Criar exposição
		String tituloExposicao = new String("ExpoJaneiro");
		String descricaoExposicao = new String("ExpoJaneiro");

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		Date dataInicioExposicao = sdf.parse("01/10/2017");
		Date dataFimExposicao = sdf.parse("01/20/2017");
		Date dataInicioSubmissao = sdf.parse("12/01/2016");
		Date dataFimSubmissao = sdf.parse("12/01/2016");
		Date dataLimiteConflitos = sdf.parse("01/05/2017");

		String localExposicao = new String("Porto");

		List<Application> listaApplications = new ArrayList<>();
		listaApplications.add(application1);
		listaApplications.add(application2);

		ApplicationSet applicationSet = new ApplicationSet(listaApplications);

		Exhibition exhibition = new Exhibition(tituloExposicao, descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, applicationSet, assignmentSet);

		List<Exhibition> exposicoesList = new ArrayList<>();
		exposicoesList.add(exhibition);
		ExhibitionSet exhibitionSet = new ExhibitionSet(exposicoesList);

		UserSet userSetConfirmados = new UserSet(utilizadoresConfirmadosList);
		UserSet userSetNaoConfirmados = new UserSet(utilizadoresNaoConfirmadosList);

		// Lista de stands
		Stand stand1 = new Stand("Stand 1");
		Stand stand2 = new Stand("Stand 2");
		List<Stand> standsList = new ArrayList<>();
		standsList.add(stand1);
		standsList.add(stand2);

		Stands stands = new Stands(standsList);
		EventCentre eventCentre = new EventCentre(userSetConfirmados, userSetNaoConfirmados, stands, exhibitionSet);

		File file = new File(utilizadorExportFilePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(eventCentre, file);
		jaxbMarshaller.marshal(eventCentre, System.out);

		XMLParser xmlParser = new XMLParser();

		Node expected = xmlParser.readXMLElementFromFile(utilizadorExportFilePath);

		Node result = xmlParser.readXMLElementFromFile(expectedCentroExposicoesImportFilePath);

		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreWhitespace(true);
		assertXMLEqual(expected.getOwnerDocument(), result.getOwnerDocument());

	}

	@Test
	public void ensureConvertFromXmlFileWorks() throws Exception {
		String expectedCentroExposicoesImportFilePath = "target/test-classes/CentroExposicoesImportExample_v0.3.xml";

		List<User> utilizadoresConfirmadosList = new ArrayList<>();
		User userConfirmado1 = new User("João Confirmado1", "joao1@empresa.pt", "joao1@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado1);
		User userConfirmado2 = new User("João Confirmado2", "joao2@empresa.pt", "joao2@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado2);

		User faeUser1 = new User("Fae1", "fae1@centro.pt", "fae1@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser1);

		User faeUser2 = new User("Fae2", "fae2@centro.pt", "fae2@centro.pt", "password");
		utilizadoresConfirmadosList.add(faeUser2);

		User userOrganizador = new User("Manuel", "manuel@centro.pt", "manuel@centro.pt", "password");
		utilizadoresConfirmadosList.add(userOrganizador);

		List<User> utilizadoresNaoConfirmadosList = new ArrayList<>();
		User userNaoConfirmado = new User("João não confirmado", "joao@empresa.pt", "joao@empresa.pt", "password");
		utilizadoresNaoConfirmadosList.add(userNaoConfirmado);

		// Lista de Organizadores
		List<Organiser> organizadoresList = new ArrayList<>();
		Organiser organiser = new Organiser(userOrganizador);
		organizadoresList.add(organiser);
		OrganiserSet organizadores = new OrganiserSet(organizadoresList);

		// Lista de FAESet
		List<FAE> faesList = new ArrayList<>();
		FAE fae1 = new FAE(faeUser1);
		faesList.add(fae1);
		FAE fae2 = new FAE(faeUser2);
		faesList.add(fae2);
		FAESet FAESet = new FAESet(faesList);

		List<String> keywordsCandidatura1 = new ArrayList<>();
		keywordsCandidatura1.add("Keyword1");
		keywordsCandidatura1.add("Keyword2");

		List<String> keywordsCandidatura2 = new ArrayList<>();
		keywordsCandidatura2.add("Keyword2");
		keywordsCandidatura2.add("Keyword3");

		/*Avaliacoes avaliacoes = new Avaliacoes(avaliacoesList);*/
		Application application1 = new Application(Boolean.TRUE, "application 1 description", 20, 5, new ArrayList<>(), keywordsCandidatura1);
		Application application2 = new Application(Boolean.FALSE, "application 2 description", 10, 15, new ArrayList<>(), keywordsCandidatura2);

		List<Assignment> atribuicoesList = new ArrayList<>();
		ArrayList<Review> avaliacoesList = new ArrayList<>();

		Assignment assignment1 = new Assignment(fae1);//, application1);
		Review review1 = new Review("text avaliação 1", 3, 5, 2, 2, 4, assignment1);

		Assignment assignment2 = new Assignment(fae2);//, application1);
		Review review2 = new Review("text avaliação 2", 2, 3, 1, 5, 3, assignment2);

		atribuicoesList.add(assignment1);
		atribuicoesList.add(assignment2);

		avaliacoesList.add(review1);
		avaliacoesList.add(review2);

		application1.setReviews(avaliacoesList);

		AssignmentSet assignmentSet = new AssignmentSet(atribuicoesList);

		//Submission submissao = new Submission(dataInicioSubmissao, dataFimSubmissao);

		// Criar exposição
		String tituloExposicao = new String("ExpoJaneiro");
		String descricaoExposicao = new String("ExpoJaneiro");

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		Date dataInicioExposicao = sdf.parse("01/10/2017");
		Date dataFimExposicao = sdf.parse("01/20/2017");
		Date dataInicioSubmissao = sdf.parse("12/01/2016");
		Date dataFimSubmissao = sdf.parse("12/01/2016");
		Date dataLimiteConflitos = sdf.parse("01/05/2017");

		String localExposicao = new String("Porto");

		List<Application> listaApplications = new ArrayList<>();
		listaApplications.add(application1);
		listaApplications.add(application2);

		ApplicationSet applicationSet = new ApplicationSet(listaApplications);

		Exhibition exhibition = new Exhibition(tituloExposicao, descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, applicationSet, assignmentSet);

		List<Exhibition> exposicoesList = new ArrayList<>();
		exposicoesList.add(exhibition);
		ExhibitionSet exhibitionSet = new ExhibitionSet(exposicoesList);

		UserSet userSetConfirmados = new UserSet(utilizadoresConfirmadosList);
		UserSet userSetNaoConfirmados = new UserSet(utilizadoresNaoConfirmadosList);

		// Lista de stands
		Stand stand1 = new Stand("Stand 1");
		Stand stand2 = new Stand("Stand 2");
		List<Stand> standsList = new ArrayList<>();
		standsList.add(stand1);
		standsList.add(stand2);

		Stands stands = new Stands(standsList);
		EventCentre eventCentre = new EventCentre(userSetConfirmados, userSetNaoConfirmados, stands, exhibitionSet);

		File file = new File(expectedCentroExposicoesImportFilePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		EventCentre result = (EventCentre) jaxbUnmarshaller.unmarshal(file);

		assertEquals(eventCentre, result);

	}

	@Test
	public void ensureConvertToAndFromXmlFileWorks() throws Exception {
		String centroExposicoesExportFilePath = "target/test-classes/CentroExposicoesOutputInput.xml";

		List<User> utilizadoresConfirmadosList = new ArrayList<>();
		User userConfirmado1 = new User("João Confirmado1", "joao1@empresa.pt", "joao1@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado1);
		User userConfirmado2 = new User("João Confirmado2", "joao2@empresa.pt", "joao2@empresa.pt", "password");
		utilizadoresConfirmadosList.add(userConfirmado2);

		User userOrganizador = new User("Manuel", "manuel@centro.pt", "manuel@centro.pt", "password");
		utilizadoresConfirmadosList.add(userOrganizador);

		List<User> utilizadoresNaoConfirmadosList = new ArrayList<>();
		User userNaoConfirmado = new User("João não confirmado", "joao@empresa.pt", "joao@empresa.pt", "password");
		utilizadoresNaoConfirmadosList.add(userNaoConfirmado);

		// Lista de Organizadores
		List<Organiser> organizadoresList = new ArrayList<>();
		Organiser organiser = new Organiser(userOrganizador);
		organizadoresList.add(organiser);
		OrganiserSet organizadores = new OrganiserSet(organizadoresList);

		// Lista de FAESet
		List<FAE> faesList = new ArrayList<>();
		User faeUser1 = new User("Fae1", "fae1@centro.pt", "fae1@centro.pt", "password");
		FAE fae1 = new FAE(faeUser1);
		faesList.add(fae1);
		User faeUser2 = new User("Fae2", "fae2@centro.pt", "fae2@centro.pt", "password");
		FAE fae2 = new FAE(faeUser2);
		faesList.add(fae2);
		FAESet FAESet = new FAESet(faesList);

		List<String> keywordsCandidatura1 = new ArrayList<>();
		keywordsCandidatura1.add("Keyword1");
		keywordsCandidatura1.add("Keyword2");

		List<String> keywordsCandidatura2 = new ArrayList<>();
		keywordsCandidatura2.add("Keyword2");
		keywordsCandidatura2.add("Keyword3");

		/*Avaliacoes avaliacoes = new Avaliacoes(avaliacoesList);*/
		Application application1 = new Application(Boolean.TRUE, "application 1 description", 20, 5, new ArrayList<>(), keywordsCandidatura1);
		Application application2 = new Application(Boolean.FALSE, "application 2 description", 10, 15, new ArrayList<>(), keywordsCandidatura2);

		List<Assignment> atribuicoesList = new ArrayList<>();
		ArrayList<Review> avaliacoesList = new ArrayList<>();

		Assignment assignment1 = new Assignment(fae1);//, application1);
		Review review1 = new Review("text avaliação 1", 3, 5, 2, 2, 4, assignment1);

		Assignment assignment2 = new Assignment(fae2);//, application1);
		Review review2 = new Review("text avaliação 2", 2, 3, 1, 5, 3, assignment2);

		atribuicoesList.add(assignment1);
		atribuicoesList.add(assignment2);

		avaliacoesList.add(review1);
		avaliacoesList.add(review2);

		application1.setReviews(avaliacoesList);

		AssignmentSet assignmentSet = new AssignmentSet(atribuicoesList);

		//Submission submissao = new Submission(dataInicioSubmissao, dataFimSubmissao);

		// Criar exposição
		String tituloExposicao = new String("ExpoJaneiro");
		String descricaoExposicao = new String("ExpoJaneiro");

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy", Locale.ENGLISH);
		Date dataInicioExposicao = sdf.parse("01/10/2017");
		Date dataFimExposicao = sdf.parse("01/20/2017");
		Date dataInicioSubmissao = sdf.parse("12/01/2016");
		Date dataFimSubmissao = sdf.parse("12/01/2016");
		Date dataLimiteConflitos = sdf.parse("01/05/2017");

		String localExposicao = new String("Porto");

		List<Application> listaApplications = new ArrayList<>();
		listaApplications.add(application1);
		listaApplications.add(application2);

		ApplicationSet applicationSet = new ApplicationSet(listaApplications);

		Exhibition exhibition = new Exhibition(tituloExposicao, descricaoExposicao, dataInicioExposicao, dataFimExposicao,
				dataInicioSubmissao, dataFimSubmissao, dataLimiteConflitos,
				localExposicao, organizadores, FAESet, applicationSet, assignmentSet);

		List<Exhibition> exposicoesList = new ArrayList<>();
		exposicoesList.add(exhibition);
		ExhibitionSet exhibitionSet = new ExhibitionSet(exposicoesList);

		UserSet userSetConfirmados = new UserSet(utilizadoresConfirmadosList);
		UserSet userSetNaoConfirmados = new UserSet(utilizadoresNaoConfirmadosList);

		// Lista de stands
		Stand stand1 = new Stand("Stand 1");
		Stand stand2 = new Stand("Stand 2");
		List<Stand> standsList = new ArrayList<>();
		standsList.add(stand1);
		standsList.add(stand2);

		Stands stands = new Stands(standsList);
		EventCentre expected = new EventCentre(userSetConfirmados, userSetNaoConfirmados, stands, exhibitionSet);

		File file = new File(centroExposicoesExportFilePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(expected, file);
		jaxbMarshaller.marshal(expected, System.out);


		file = new File(centroExposicoesExportFilePath);
		jaxbContext = JAXBContext.newInstance(EventCentre.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		EventCentre result = (EventCentre) jaxbUnmarshaller.unmarshal(file);

		assertEquals(expected, result);

	}

	@Before
	public void setUp() throws Exception {
		for (int i = 1; i < 50; i++) {
			keywordsList.add("Keyword" + i);
		}


	}
}