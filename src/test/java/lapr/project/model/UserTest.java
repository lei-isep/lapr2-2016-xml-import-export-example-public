package lapr.project.model;

import lapr.project.utils.XMLParser;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.Test;
import org.w3c.dom.Node;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;


import static junit.framework.TestCase.assertEquals;
import static org.custommonkey.xmlunit.XMLAssert.assertXMLEqual;

/**
 * Created by Nuno Bettencourt [NMB] on 10/06/16.
 */
public class UserTest {

	@Test
	public void ensureConvertToXmlWorks() throws Exception {
		String utilizadorExportFilePath = "target/test-classes/UtilizadorOutput.xml";
		String expectedUtilizadorImportFilePath = "target/test-classes/UtilizadorImportExample.xml";

		User user = new User("João", "joao@empresa.pt", "joao@empresa.pt", "password");

		File file = new File(utilizadorExportFilePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
		Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

		// output pretty printed
		jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

		jaxbMarshaller.marshal(user, file);
		jaxbMarshaller.marshal(user, System.out);

		XMLParser xmlParser = new XMLParser();

		Node expected = xmlParser.readXMLElementFromFile(utilizadorExportFilePath);

		Node result = xmlParser.readXMLElementFromFile(expectedUtilizadorImportFilePath);

		XMLUnit.setIgnoreAttributeOrder(true);
		XMLUnit.setIgnoreComments(true);
		XMLUnit.setIgnoreWhitespace(true);
		assertXMLEqual(expected.getOwnerDocument(), result.getOwnerDocument());
	}

	@Test
	public void ensureConvertToObjectWorks() throws Exception {
		String expectedUtilizadorImportFilePath = "target/test-classes/UtilizadorImportExample.xml";

		User expected = new User("João", "joao@empresa.pt", "joao@empresa.pt", "password");

		File file = new File(expectedUtilizadorImportFilePath);
		JAXBContext jaxbContext = JAXBContext.newInstance(User.class);
		Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
		User result = (User) jaxbUnmarshaller.unmarshal(file);

		assertEquals(expected, result);
	}

}