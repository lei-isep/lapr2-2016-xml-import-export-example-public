package lapr.project.model;

import lapr.project.utils.StringUtil;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

/**
 * Class to demonstrate a Application simple example.
 * @author Nuno Bettencourt [nmb@isep.ipp.pt] on 29/05/16.
 */
public class ApplicationExampleTest {

	/**
	 * StringUtil variable to access utils for Strings.
	 */
	private StringUtil stringUtil = new StringUtil();

	/**
	 * Get OS independent line break.
	 *
	 * @return OS independent line break "%n".
	 */
	private String getLineBreak() {
		return stringUtil.getLineBreak();
	}

	@Test
	public void ensureAddKeywordIsWorking() throws Exception {
		List<KeywordExample> expectedKeywordExampleList = new ArrayList<>();
		expectedKeywordExampleList.add(new KeywordExample("Doors"));

		AnotherApplicationExample candidatura = new AnotherApplicationExample("MyCandidatura", new ArrayList<>());
		candidatura.addKeyword(new KeywordExample("Doors"));

		List<KeywordExample> resultList = candidatura.getKeywordList();

		assertArrayEquals(expectedKeywordExampleList.toArray(), resultList.toArray());

	}

	@Test
	public void ensureXMLElementExportToStringIsValid() throws Exception {
		String expected = "<application>" + getLineBreak()  +
				"<description>MyCandidatura</description>" + getLineBreak()  +
				"<keywords>" + getLineBreak()  +
				"<keyword>" + getLineBreak()  +
				"<value>Doors</value>" + getLineBreak()  +
				"</keyword>" + getLineBreak()  +
				"<keyword>" + getLineBreak()  +
				"<value>Windows</value>" + getLineBreak()  +
				"</keyword>" + getLineBreak()  +
				"</keywords>" + getLineBreak()  +
				"</application>" + getLineBreak();

		List<KeywordExample> keywordList = new ArrayList<>();
		keywordList.add(new KeywordExample("Doors"));
		keywordList.add(new KeywordExample("Windows"));
		AnotherApplicationExample anotherApplicationExample = new AnotherApplicationExample("MyCandidatura", keywordList);
		String result = anotherApplicationExample.exportContentToString();
		assertEquals(expected, result);
	}

	@Test
	public void ensureImportFromXMLElementNodeIsValid() throws Exception {
		List<KeywordExample> keywordExampleList = new ArrayList<>();
		keywordExampleList.add(new KeywordExample("Doors"));
		keywordExampleList.add(new KeywordExample("Windows"));

		AnotherApplicationExample expected = new AnotherApplicationExample("MyCandidatura", keywordExampleList);

		DocumentBuilderFactory factory =
				DocumentBuilderFactory.newInstance();

		//Create document builder
		DocumentBuilder builder = factory.newDocumentBuilder();

		//Obtain a new document
		Document document = builder.newDocument();

		//Create root element
		Element elementCandidatura = document.createElement("application");

		//Create a sub-element
		Element elementDescription = document.createElement("description");

		//Set the sub-element value
		elementDescription.setTextContent("MyCandidatura");

		//Add sub-element to root element
		elementCandidatura.appendChild(elementDescription);

		//Create a sub-element
		Element elementKeywords = document.createElement("keywords");

		//iterate over keywords
		for (KeywordExample keyword : keywordExampleList) {
			Node keywordNode = keyword.exportContentToXMLNode();
			elementKeywords.appendChild(document.importNode(keywordNode, true));
		}

		elementCandidatura.appendChild(elementKeywords);

		//Add root element to document
		document.appendChild(elementCandidatura);

		AnotherApplicationExample result = new AnotherApplicationExample();
		result = result.importContentFromXMLNode(elementCandidatura);

		assertEquals(expected, result);
	}

	@Test
	public void ensureSameContentObjectsAreEqual() {
		String description = "MyCandidatura";

		List<KeywordExample> keywords = new ArrayList<>();
		keywords.add(new KeywordExample("Doors"));
		keywords.add(new KeywordExample("Windows"));

		AnotherApplicationExample expected = new AnotherApplicationExample(description, keywords);
		AnotherApplicationExample result = new AnotherApplicationExample(description, keywords);

		assertEquals(expected, result);
	}

	@Test
	public void ensureSameObjectIsEqual() {
		String description = "MyCandidatura";

		List<KeywordExample> keywords = new ArrayList<>();
		keywords.add(new KeywordExample("Doors"));
		keywords.add(new KeywordExample("Windows"));

		AnotherApplicationExample expected = new AnotherApplicationExample(description, keywords);

		assertEquals(expected, expected);
	}

	@Test
	public void ensureDifferentObjectsAreNotEqual() {
		String description = "MyCandidatura";

		List<KeywordExample> keywords = new ArrayList<>();
		keywords.add(new KeywordExample("Doors"));
		keywords.add(new KeywordExample("Windows"));

		AnotherApplicationExample expected = new AnotherApplicationExample(description, keywords);

		Object result = new Object();
		assertNotEquals(expected, result);
	}

	@Test
	public void ensureDifferentDescriptionMakeObjectsNotEqual() {
		String description1 = "MyCandidatura1";
		String description2 = "MyCandidatura2";

		List<KeywordExample> keywords = new ArrayList<>();
		keywords.add(new KeywordExample("Doors"));
		keywords.add(new KeywordExample("Windows"));

		AnotherApplicationExample expected = new AnotherApplicationExample(description1, keywords);
		AnotherApplicationExample result = new AnotherApplicationExample(description2, keywords);

		assertNotEquals(expected, result);
	}

	@Test
	public void ensureHashCodeIsCorrect() {
		String description = "MyCandidatura";

		List<KeywordExample> keywords = new ArrayList<>();
		keywords.add(new KeywordExample("Doors"));
		keywords.add(new KeywordExample("Windows"));

		AnotherApplicationExample anotherApplicationExample = new AnotherApplicationExample(description, keywords);

		int expected = 461375881;
		int result = anotherApplicationExample.hashCode();
		assertEquals(expected, result);

	}


}